import {START_ROUND_FAILURE, START_ROUND_REQUEST, START_ROUND_SUCCESS,} from './actionTypes'

// start request
export function startRound(payload) {
    return {
        type: START_ROUND_REQUEST,
        payload
    };
}

// on successful
export function startRoundSuccess(payload) {
    return {
        type: START_ROUND_SUCCESS,
        payload
    };
}

// on fail
export function startRoundFailure(payload) {
    return {
        type: START_ROUND_FAILURE,
        payload
    };
}