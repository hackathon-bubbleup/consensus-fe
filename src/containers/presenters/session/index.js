import React from "react";
import {Container, Grid, Header, Icon, Menu} from "semantic-ui-react";
import Participants from "./containers/Participants";
import connect from "react-redux/es/connect/connect";
import {Route, Switch} from "react-router";
import Trivias from "./containers/Trivias";
import Rounds from "./containers/Rounds";

class PresenterSession extends React.Component {
    render() {
        return (
            <div>
                <Menu fixed='top' inverted>
                    <Container>
                        <Menu.Item header>
                            CON-SEN-SUS
                        </Menu.Item>
                    </Container>
                </Menu>
                <Container fluid style={{paddingTop: '2em'}}>
                    <Grid celled style={{minHeight: 600}}>
                        <Grid.Row>
                            <Grid.Column width={11}>
                                <Switch>
                                    <Route exact path={`${this.props.match.path}`} component={Trivias}/>
                                    <Route exact path={`${this.props.match.path}/round`} component={Rounds}/>
                                </Switch>
                            </Grid.Column>
                            <Grid.Column width={5}>
                                <Header as='h5' attached='top'>
                                    <Icon name={'users'}/>
                                    Participants
                                </Header>
                                <Participants/>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Container>
            </div>

        );
    }
}

const mapStateToProps = ({sessions}) => ({
    session: sessions.session
});

const mapDispatchToProps = (dispatch) => {
    return {}
};


export default connect(mapStateToProps, mapDispatchToProps)(PresenterSession);