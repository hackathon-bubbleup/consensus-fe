import React from "react";
import {Button, Divider, Header, Icon, Segment} from "semantic-ui-react";
import {bindActionCreators} from "redux";
import {voteAnswer} from "../../../../actions/sessionActions";
import {push} from "connected-react-router";
import {connect} from "react-redux";

class Vote extends React.Component {
    constructor(props) {
        super(props);
        this.voteFor = this._voteFor.bind(this);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.moveToResult !== this.props.moveToResult && this.props.moveToResult) {
            this.props.push(`${this.props.match.url}/result`);
        }
    }

    _voteFor = (answer) => {
        const {session, me, voteAnswer} = this.props;
        voteAnswer({
            sessionName: session.topicName,
            answerText: answer.text,
            participant: me
        })
    };

    render() {
        const {question, answers} = this.props;
        return (
            <div>
                <Segment attached>
                    <Header as='h4' attached={'top'}>
                        <Icon name='question circle'/>
                        <Header.Content>Question</Header.Content>
                    </Header>
                    <Segment size={"huge"} attached>
                        {question}
                    </Segment>
                    <Header as='h4' attached={'top'}>
                        <Icon name='hashtag'/>
                        <Header.Content>Vote for answer</Header.Content>
                    </Header>
                    <Segment attached>
                        {
                            answers.map((ans, idx) => (
                                <div key={ans.index}>
                                    <Button fluid color={'teal'} onClick={() => this.voteFor(ans)}>
                                        {ans.text}
                                    </Button>
                                    {(idx + 1) < answers.length && <Divider horizontal>Or</Divider>}
                                </div>
                            ))
                        }
                    </Segment>
                </Segment>
            </div>
        );
    }
}

const mapStateToProps = ({sessions, participant}) => ({
    session: (sessions.session && sessions.session) || {},
    participants: (sessions.session && sessions.session.participants) || [],
    question: (sessions.round && sessions.round.question) || "",
    answers: (sessions.round && sessions.round.answers) || [],
    moveToResult: (sessions.moveToResult) || false,
    me: participant
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        voteAnswer,
        push
    }, dispatch)
};


export default connect(mapStateToProps, mapDispatchToProps)(Vote);