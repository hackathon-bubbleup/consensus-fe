import React from "react";
import {Header, Icon, Progress, Segment, Transition} from "semantic-ui-react";
import connect from "react-redux/es/connect/connect";

class ProgressResult extends React.Component {
    constructor(props) {
        super(props);
        this.getPercentage = this._getPercentage.bind(this);
    }

    _getPercentage = (answer) => {
        return (answer.voteCount / this.props.participants.length) * 100;
    };

    render() {
        const {question, answers} = this.props;
        return (
            <div>
                <Segment attached>
                    <Header as='h4' attached={'top'}>
                        <Icon name='question circle'/>
                        <Header.Content>Question</Header.Content>
                    </Header>
                    <Segment size={"huge"} attached>
                        {question}
                    </Segment>

                    <Header as='h4' attached={'top'}>
                        <Icon name='hashtag'/>
                        <Header.Content>Answers</Header.Content>
                    </Header>
                    <Segment attached>
                        {
                            answers.map(ans => (
                                <Transition visible={ans.status === "KEEP"} animation='scale' duration={500} key={ans.index}>
                                    <Progress percent={this.getPercentage(ans)} indicating>
                                        {ans.text}
                                    </Progress>
                                </Transition>
                            ))
                        }
                    </Segment>
                </Segment>
            </div>
        );
    }
}

const mapStateToProps = ({sessions}) => ({
    participants: (sessions.session && sessions.session.participants) || [],
    question: (sessions.round && sessions.round.question) || "",
    answers: (sessions.round && sessions.round.answers) || []
});

const mapDispatchToProps = (dispatch) => {
    return {}
};


export default connect(mapStateToProps, mapDispatchToProps)(ProgressResult);