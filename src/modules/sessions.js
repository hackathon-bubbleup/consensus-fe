import {
    CREATE_SESSION_FAILURE,
    CREATE_SESSION_REQUEST,
    CREATE_SESSION_SUCCESS,
    CREATE_TRIVIA_FAILURE,
    CREATE_TRIVIA_REQUEST,
    CREATE_TRIVIA_SUCCESS, END_ROUND_FAILURE, END_ROUND_REQUEST, END_ROUND_SUCCESS,
    EVENT_ADD_PARTICIPANT, EVENT_ROUND_ENDED,
    EVENT_ROUND_STARTED,
    EVENT_VOTE_RECEIVED,
    JOIN_SESSION_FAILURE,
    JOIN_SESSION_REQUEST,
    JOIN_SESSION_SUCCESS,
    VOTE_ANSWER_FAILURE,
    VOTE_ANSWER_REQUEST,
    VOTE_ANSWER_SUCCESS
} from '../actions/actionTypes'

const initialState = {};

const getNewAnswers = (newAnswers, triviaState) => {
    return newAnswers.map(a => {
        const newAnswer = triviaState.find(value => value.text.toLowerCase() === a.text.toLowerCase());
        if (newAnswer) {
            a.status = newAnswer.status;
            a.voteCount = newAnswer.voteCount;
        }
        return a;
    })
};

export default (state = initialState, action) => {
    switch (action.type) {
        case CREATE_SESSION_REQUEST:
            return {
                ...state,
                isCreatingSession: true
            };

        case CREATE_SESSION_SUCCESS:
            return {
                ...state,
                session: action.payload,
                isCreatingSession: false,
                roundEnded: false
            };

        case CREATE_SESSION_FAILURE:
            return {
                ...state,
                error: action.payload,
                isCreatingSession: false
            };


        case JOIN_SESSION_REQUEST:
            return {
                ...state,
                isJoiningSession: true
            };

        case JOIN_SESSION_SUCCESS:
            return {
                ...state,
                session: action.payload,
                isJoiningSession: false
            };

        case JOIN_SESSION_FAILURE:
            return {
                ...state,
                error: action.payload,
                isJoiningSession: false
            };

        case END_ROUND_REQUEST:
            return {
                ...state,
                isEndingRound: true
            };

        case EVENT_ROUND_ENDED:
        case END_ROUND_SUCCESS:
            const {round, ...remainingState} = state;
            const {answers, ...remainingRoundAttr} = round;
            return {
                ...remainingState,
                round: {
                    ...remainingRoundAttr,
                    answers: [...getNewAnswers(answers, action.payload.aggregateAnswers)]
                },
                isEndingRound: false,
                roundEnded: true
            };

        case END_ROUND_FAILURE:
            return {
                ...state,
                error: action.payload,
                isEndingRound: false
            };


        case VOTE_ANSWER_REQUEST:
            return {
                ...state,
                isVoting: true
            };

        case VOTE_ANSWER_SUCCESS:
            if (state.round) {
                const {round, ...remainingState} = state;
                const {answers, ...remainingRoundAttr} = round;
                let newAnswers = answers || [];
                if (action.payload && action.payload.length > 0) {
                    newAnswers = getNewAnswers(newAnswers, action.payload);
                }
                return {
                    round: {
                        answers: [...newAnswers],
                        ...remainingRoundAttr
                    },
                    ...remainingState,
                    isVoting: false,
                    moveToResult: true
                }
            } else {
                return state;
            }

        case VOTE_ANSWER_FAILURE:
            return {
                ...state,
                error: action.payload,
                isVoting: false
            };


        case CREATE_TRIVIA_REQUEST:
            return {
                ...state,
                isCreatingTrivia: true
            };

        case CREATE_TRIVIA_SUCCESS:
            if (state.session) {
                const {session, ...remainingState} = state;
                const trivias = session.trivias || [];
                session.trivias = [...trivias, action.payload];
                return {
                    session,
                    ...remainingState
                }
            } else {
                return {
                    ...state,
                    session: {
                        trivias: [action.payload]
                    }
                }
            }

        case CREATE_TRIVIA_FAILURE:
            return {
                ...state,
                error: action.payload,
                isCreatingTrivia: false
            };


        case EVENT_ROUND_STARTED:
            if (state.session) {
                const {session, ...remainingState} = state;
                const {status, ...remainingSessionAttr} = session;
                const participants = session.participants || [];
                return {
                    session: {
                        status: "LOCKED",
                        ...remainingSessionAttr
                    },
                    round: {
                        question: action.payload.question,
                        answers: action.payload.answers.map(a => ({...a, voteCount: 0})),
                        participants: [...participants]
                    },
                    ...remainingState
                }
            } else {
                return {
                    ...state,
                    session: {
                        status: "OPEN"
                    }
                }
            }



        case EVENT_ADD_PARTICIPANT:
            if (state.session) {
                const {session, ...remainingState} = state;
                const participants = session.participants || [];
                session.participants = [...participants, action.payload];
                return {
                    session,
                    ...remainingState
                }
            } else {
                return {
                    ...state,
                    session: {
                        participants: [action.payload]
                    }
                }
            }

        case EVENT_VOTE_RECEIVED:
            if (state.round) {
                const {round, ...remainingState} = state;
                const {answers, ...remainingRoundAttr} = round;
                let newAnswers = answers || [];
                if (action.payload && action.payload.triviaState) {
                    newAnswers = getNewAnswers(newAnswers, action.payload.triviaState);
                }

                return {
                    round: {
                        answers: [...newAnswers],
                        ...remainingRoundAttr
                    },
                    ...remainingState
                }
            } else {
                return state;
            }

        default:
            return state
    }
}