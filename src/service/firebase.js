import firebase from 'firebase/app';
import 'firebase/messaging';

var config = {
    apiKey: "AIzaSyAaQJ82974csiG8EiTuyL71tOAbYXLIou8",
    authDomain: "consensus-hack2018.firebaseapp.com",
    databaseURL: "https://consensus-hack2018.firebaseio.com",
    projectId: "consensus-hack2018",
    storageBucket: "consensus-hack2018.appspot.com",
    messagingSenderId: "162268521747"
};

firebase.initializeApp(config);

export default firebase;