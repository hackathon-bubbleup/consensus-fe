import {combineEpics} from 'redux-observable';
import sessionRepos from './sessionRepos'
import processFirebaseMessage from "./firebaseMessageProcessor";

const epics = combineEpics(
    sessionRepos.createSession,
    sessionRepos.joinSession,
    sessionRepos.createTrivia,
    sessionRepos.voteAnswer,
    sessionRepos.endRound,
    processFirebaseMessage
);

export default epics