import React from "react";
import {Button, Header, Icon, Progress, Segment, Transition} from "semantic-ui-react";
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import {endRoundRequest} from "../../../../actions/sessionActions";
import {push} from "connected-react-router";

class Rounds extends React.Component {
    constructor(props) {
        super(props);
        this.getPercentage = this._getPercentage.bind(this);
        this.startRound = this._startRound.bind(this);
        this.endRound = this._endRound.bind(this);
    }

    _getPercentage = (answer) => {
        return (answer.voteCount / this.props.participants.length) * 100;
    };

    _startRound = () => {
        console.log("start the next round");
    };

    _endRound = () => {
        const {endRoundRequest, match, presenter} = this.props;
        endRoundRequest({
            presenter: presenter,
            sessionName: match.params.sessionName,
        });
    };

    componentDidUpdate(prevProps){
        if (prevProps.isEndingRound !== this.props.isEndingRound){
            if(this.props.roundEnded){

            }
        }
    }

    _getButton(){
        if (this.props.roundEnded){
            return (
                <Button floated='right' color={'green'} onClick={this.startRound}>Start round</Button>
            )
        } else {
            return (
                <Button floated='right' color={'yellow'} onClick={this.endRound}>End round</Button>
            )
        }

    }

    render() {
        const {question, answers} = this.props;
        return (
            <div>
                <Segment attached>
                    <Header as='h4' attached={'top'}>
                        <Icon name='question circle'/>
                        <Header.Content>Question</Header.Content>
                    </Header>
                    <Segment size={"huge"} attached>
                        {question}
                    </Segment>

                    <Header as='h4' attached={'top'}>
                        <Icon name='hashtag'/>
                        <Header.Content>Answers</Header.Content>
                    </Header>
                    <Segment attached>
                        {
                            answers.map(ans => (
                                <Transition visible={ans.status === "KEEP"} animation='scale' duration={500} key={ans.index}>
                                    <Progress percent={this.getPercentage(ans)} indicating>
                                        {ans.text}
                                    </Progress>
                                </Transition>
                            ))
                        }
                    </Segment>
                </Segment>
                <Segment attached='bottom' clearing>
                    {this._getButton()}
                </Segment>
            </div>
        );
    }
}

const mapStateToProps = ({sessions}) => ({
    participants: (sessions.session && sessions.session.participants) || [],
    presenter: (sessions.session && sessions.session.presenter) || {},
    question: (sessions.round && sessions.round.question) || "",
    answers: (sessions.round && sessions.round.answers) || [],
    isEndingRound: (sessions.isEndingRound && sessions.isEndingRound) || false,
    roundEnded: (sessions.roundEnded && sessions.roundEnded) || false
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        endRoundRequest,
        push
    }, dispatch)
};


export default connect(mapStateToProps, mapDispatchToProps)(Rounds);