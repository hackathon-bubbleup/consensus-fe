# production environment
FROM nginx:1.15.6-alpine
RUN mkdir /var/www
COPY build /var/www
COPY nginx-default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]