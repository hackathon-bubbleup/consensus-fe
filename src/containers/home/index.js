import React from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {Button, Form, Grid, Header, Message, Segment} from 'semantic-ui-react'
import {Link} from "react-router-dom";
import {joinSession} from "../../actions/sessionActions";
import {push} from "connected-react-router";

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.joinSession = this._joinSession.bind(this);
        this.handleEvent = this._handleEvent.bind(this);

        this.state = {
            sessionName: "",
            nickname: ""
        }
    }

    componentDidUpdate() {
        const {sessions, push} = this.props;
        if (sessions.session && !sessions.isJoiningSession) {
            push('/' + sessions.session.topicName);
        }
    }

    _joinSession = () => {
        const {nickname, sessionName} = this.state;
        this.props.joinSession({
            sessionName,
            participant: {
                nickname: nickname,
                fcmToken: this.props.fcmToken
            }
        })
    };

    _handleEvent = (event, data) => {
        this.setState({
            [event.target.id]: data.value
        })
    };

    render() {
        const {nickname, sessionName} = this.state;

        return (
            <div className='login-form'>
                {/*
      Heads up! The styles below are necessary for the correct render of this example.
      You can do same with CSS, the main idea is that all the elements up to the `Grid`
      below must have a height of 100%.
    */}
                <style>{`
      body > div,
      body > div > div,
      body > div > div > div.login-form {
        height: 100%;
      }
    `}</style>
                <Grid textAlign='center' style={{height: '100%'}} verticalAlign='middle'>
                    <Grid.Column style={{maxWidth: 450}}>
                        <Header as='h2' color='teal' textAlign='center'>
                            Join an ongoing session
                        </Header>
                        <Form size='large'>
                            <Segment stacked>
                                <Form.Input fluid icon='user' iconPosition='left'
                                            value={nickname}
                                            id={"nickname"}
                                            placeholder='Nickname'
                                            onChange={this.handleEvent}
                                />
                                <Form.Input fluid icon='user' iconPosition='left'
                                            value={sessionName}
                                            id={"sessionName"}
                                            placeholder='Session name'
                                            onChange={this.handleEvent}
                                />
                                <Button color='teal' fluid size='large' onClick={this.joinSession}>
                                    Join Session
                                </Button>
                            </Segment>
                        </Form>
                        <Message>
                            Presenting? <Link to="/present">Create Session</Link>
                        </Message>
                    </Grid.Column>
                </Grid>
            </div>
        )
    }
}

const mapStateToProps = ({sessions, firebase}) => ({
    fcmToken: firebase.token,
    sessions
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        joinSession,
        push
    }, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);