import {ajax} from 'rxjs/observable/dom/ajax';
import {ofType} from 'redux-observable';
import {catchError, flatMap, map, mergeMap} from 'rxjs/operators';
import {
    CREATE_SESSION_REQUEST,
    CREATE_TRIVIA_REQUEST,
    END_ROUND_REQUEST,
    JOIN_SESSION_REQUEST,
    VOTE_ANSWER_REQUEST
} from '../actions/actionTypes';
import {
    createParticipant,
    createSessionFailure,
    createSessionSuccess,
    createTriviaFailure,
    createTriviaSuccess,
    endRoundFailure,
    endRoundSuccess,
    joinSessionFailure,
    joinSessionSuccess,
    voteAnswerFailure,
    voteAnswerSuccess
} from '../actions/sessionActions';

// Also now using v6 pipe operators
const createSession = (action$, state$) => action$.pipe(
    ofType(CREATE_SESSION_REQUEST),
    flatMap(action => {
        let apiUrl = `/api/sessions`;
        return ajax
            .post(apiUrl, action.payload, {
                "Content-Type": "application/json"
            })
            .pipe(
                map(result => result.response),
                map(response => createSessionSuccess(response)),
                catchError(error => createSessionFailure(error.xhr.response))
            );
    })
);

const createTrivia = (action$, state$) => action$.pipe(
    ofType(CREATE_TRIVIA_REQUEST),
    flatMap(action => {
        let apiUrl = `/api/sessions/${action.payload.sessionId}/start`;
        return ajax
            .post(apiUrl, action.payload.trivia, {
                "Content-Type": "application/json"
            })
            .pipe(
                map(result => result.response),
                map(response => createTriviaSuccess(response)),
                catchError(error => createTriviaFailure(error.xhr.response))
            );
    })
);

const joinSession = (action$, state$) => action$.pipe(
    ofType(JOIN_SESSION_REQUEST),
    flatMap(action => {
        let apiUrl = `/api/sessions/${action.payload.sessionName}/join`;
        return ajax
            .post(apiUrl, action.payload.participant, {
                "Content-Type": "application/json",
                "Accept": "application/json"
            })
            .pipe(
                map(result => result.response),
                mergeMap(response => [joinSessionSuccess(response), createParticipant(action.payload.participant)]),
                catchError(error => joinSessionFailure(error.xhr.response))
            );
    })
);


const voteAnswer = (action$, state$) => action$.pipe(
    ofType(VOTE_ANSWER_REQUEST),
    flatMap(action => {
        let apiUrl = `/api/answers`;
        return ajax
            .put(apiUrl, action.payload, {
                "Content-Type": "application/json",
                "Accept": "application/json"
            })
            .pipe(
                map(result => result.response),
                map(response => voteAnswerSuccess(response)),
                catchError(error => voteAnswerFailure(error.xhr.response))
            );
    })
);

const endRound = (action$, state$) => action$.pipe(
    ofType(END_ROUND_REQUEST),
    flatMap(action => {
        let apiUrl = `/api/rounds/${action.payload.sessionName}/finish`;
        return ajax
            .post(apiUrl, action.payload.presenter, {
                "Content-Type": "application/json"
            })
            .pipe(
                map(result => result.response),
                map(response => endRoundSuccess(response)),
                catchError(error => endRoundFailure(error.xhr.response))
            );
    })
);

export default {
    createSession,
    joinSession,
    createTrivia,
    endRound,
    voteAnswer
};