import {
    CREATE_SESSION_FAILURE,
    CREATE_SESSION_REQUEST,
    CREATE_SESSION_SUCCESS,
    CREATE_TRIVIA_FAILURE,
    CREATE_TRIVIA_REQUEST,
    CREATE_TRIVIA_SUCCESS, END_ROUND_FAILURE,
    END_ROUND_REQUEST,
    END_ROUND_SUCCESS,
    JOIN_SESSION_FAILURE,
    JOIN_SESSION_REQUEST,
    JOIN_SESSION_SUCCESS,
    PARTICIPANT_CREATE_PARTICIPANT,
    VOTE_ANSWER_FAILURE,
    VOTE_ANSWER_REQUEST,
    VOTE_ANSWER_SUCCESS,
} from './actionTypes'

// start request
export function createSession(payload) {
    return {
        type: CREATE_SESSION_REQUEST,
        payload
    };
}

// on successful
export function createSessionSuccess(payload) {
    return {
        type: CREATE_SESSION_SUCCESS,
        payload
    };
}

// on fail
export function createSessionFailure(payload) {
    return {
        type: CREATE_SESSION_FAILURE,
        payload
    };
}

// start request
export function createTrivia(payload) {
    return {
        type: CREATE_TRIVIA_REQUEST,
        payload
    };
}

// on successful
export function createTriviaSuccess(payload) {
    return {
        type: CREATE_TRIVIA_SUCCESS,
        payload
    };
}

// on fail
export function createTriviaFailure(payload) {
    return {
        type: CREATE_TRIVIA_FAILURE,
        payload
    };
}

// start request
export function joinSession(payload) {
    return {
        type: JOIN_SESSION_REQUEST,
        payload
    };
}

// on successful
export function joinSessionSuccess(payload) {
    return {
        type: JOIN_SESSION_SUCCESS,
        payload
    };
}

// on fail
export function joinSessionFailure(payload) {
    return {
        type: JOIN_SESSION_FAILURE,
        payload
    };
}


// start request
export function endRoundRequest(payload) {
    return {
        type: END_ROUND_REQUEST,
        payload
    };
}

// on successful
export function endRoundSuccess(payload) {
    return {
        type: END_ROUND_SUCCESS,
        payload
    };
}

// on fail
export function endRoundFailure(payload) {
    return {
        type: END_ROUND_FAILURE,
        payload
    };
}

// start request
export function voteAnswer(payload) {
    return {
        type: VOTE_ANSWER_REQUEST,
        payload
    };
}

// on successful
export function voteAnswerSuccess(payload) {
    return {
        type: VOTE_ANSWER_SUCCESS,
        payload
    };
}

// on fail
export function voteAnswerFailure(payload) {
    return {
        type: VOTE_ANSWER_FAILURE,
        payload
    };
}

export function createParticipant(payload) {
    return {
        type: PARTICIPANT_CREATE_PARTICIPANT,
        payload
    };
}
