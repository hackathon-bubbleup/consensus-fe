import {PROCESS_FCM_MESSAGE, STORE_FCM_TOKEN} from '../actions/actionTypes'

const initialState = {};

export default (state = initialState, action) => {
    switch (action.type) {
        case STORE_FCM_TOKEN:
            return {
                ...state,
                ...action.payload
            };
        case PROCESS_FCM_MESSAGE:
            return {
                ...state,
                lastMessage: action.payload
            };
        default:
            return state
    }
}
