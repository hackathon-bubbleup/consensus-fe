import {applyMiddleware, compose, createStore} from 'redux';
import {connectRouter, routerMiddleware} from 'connected-react-router';
import createHistory from 'history/createBrowserHistory';
import rootReducer from './modules';
import {createEpicMiddleware} from 'redux-observable';
import queryString from 'query-string';

import rootEpic from "./epics"

export const history = createHistory();

const epicMiddleware = createEpicMiddleware({
    dependencies: {
        queryString
    }
});


const initialState = {};
const enhancers = [];
const middleware = [];
middleware.push(applyMiddleware(epicMiddleware));
middleware.push(applyMiddleware(routerMiddleware(history)));

if (process.env.NODE_ENV === 'development') {
    const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__;

    if (typeof devToolsExtension === 'function') {
        enhancers.push(devToolsExtension())
    }
}

const composedEnhancers = compose(
    ...middleware,
    ...enhancers
);

const store = createStore(
    connectRouter(history)(rootReducer),
    initialState,
    composedEnhancers
);
epicMiddleware.run(rootEpic);



export default store;