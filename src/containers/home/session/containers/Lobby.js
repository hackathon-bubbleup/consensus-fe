import React from "react";
import {Icon, List, Message, Segment} from "semantic-ui-react";
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import {push} from "connected-react-router";


class Lobby extends React.Component {
    componentDidUpdate(prevProps){
        if (prevProps.sessionState !== this.props.sessionState && this.props.sessionState === "LOCKED"){
            this.props.push(`${this.props.match.url}/round`);
        }
    }

    render() {
        const {participants} = this.props;
        if (participants.length === 0) {
            return (
                <Segment attached>
                    <Message icon>
                        <Icon name='circle notched' loading/>
                        <Message.Content>
                            <Message.Header>Just a minute</Message.Header>
                            Waiting for participants
                        </Message.Content>
                    </Message>
                </Segment>
            )
        }

        return (
            <Segment attached>
                <List divided verticalAlign='middle'>
                    {
                        participants.map(p => {
                            return (
                                <List.Item key={p.nickname}>
                                    <List.Content>
                                        <List.Header>{p.nickname}</List.Header>
                                    </List.Content>
                                </List.Item>
                            )
                        })
                    }
                </List>
            </Segment>
        );
    }
}

const mapStateToProps = ({sessions}) => ({
    participants: (sessions.session && sessions.session.participants) || [],
    sessionState: (sessions.session && sessions.session.status) || []
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        push
    }, dispatch)
};


export default connect(mapStateToProps, mapDispatchToProps)(Lobby);