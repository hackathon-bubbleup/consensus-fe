import {ofType} from 'redux-observable';
import {map} from 'rxjs/operators';
import {PROCESS_FCM_MESSAGE} from '../actions/actionTypes';
import {
    processMessage_AddParticipant,
    processMessage_EndRound,
    processMessage_StartRound,
    processMessage_VoteReceived,
    processMessageFailure
} from "../actions/firebaseActions";

// Also now using v6 pipe operators
const processFirebaseMessage = (action$, state$) => action$.pipe(
    ofType(PROCESS_FCM_MESSAGE),
    map(action => {
        if (action.payload.data && action.payload.data.TYPE && action.payload.data.message) {
            const message = JSON.parse(action.payload.data.message);
            switch (action.payload.data.TYPE) {
                case "PARTICIPANT_JOINED":
                    return processMessage_AddParticipant(message);
                case "SESSION_STARTED":
                    return processMessage_StartRound(message);
                case "END_ROUND":
                    return processMessage_EndRound(message);
                case "USER_ANSWER":
                    return processMessage_VoteReceived(message);
                default:
                    return processMessageFailure(action);
            }
        }

        console.log("stream of messages -> %o", action);
        return processMessageFailure(action);
    })
);

export default processFirebaseMessage;