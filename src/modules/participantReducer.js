import {PARTICIPANT_CREATE_PARTICIPANT} from '../actions/actionTypes'

const initialState = {};

export default (state = initialState, action) => {
    switch (action.type) {
        case PARTICIPANT_CREATE_PARTICIPANT:
            return {
                ...state,
                ...action.payload
            };
        default:
            return state
    }
}
