import React from 'react';
import firebase from './service/firebase';
import {Container, Dimmer, Loader, Segment} from "semantic-ui-react";
import {bindActionCreators} from "redux";
import {processMessage, storeToken} from "./actions/firebaseActions";
import {connect} from "react-redux";

const messaging = firebase.messaging();

class Initializer extends React.Component {
    constructor(props) {
        super(props);
        this.hookFirebase = this._hookFirebase.bind(this);
        this.listenToMessage = this._listenToMessage.bind(this);
        this.state = {
            initializing: true
        }
    }

    componentDidMount() {
        this.hookFirebase();
    }

    componentDidUpdate(prevProps) {
        if (!prevProps.firebaseToken && this.props.firebaseToken) {
            this.setState({
                initializing: false,
            });
        }
    }

    _listenToMessage() {
        messaging.onMessage((payload) => {
            console.log('Message received --- foreground ', payload);
            this.props.processMessage(payload);
        });
    }

    _hookFirebase() {
        messaging.requestPermission()
            .then(() => {
                console.log('Notification permission granted.');
                return messaging.getToken();
            })
            .then(currentToken => {
                //TODO: store in localStorage
                if (currentToken) {
                    console.log(currentToken);
                    this.listenToMessage();
                    this.props.storeToken({token: currentToken});
                    this.sendTokenToServer(currentToken);
                    console.log('Listening for messages');
                } else {
                    // Show permission request.
                    console.log('No Instance ID token available. Request permission to generate one.');
                    this.hookFirebase();
                }
            })
            .catch(function (err) {
                console.log('Unable to get permission to notify.', err);
            });
    }

    // Send the Instance ID token your application server, so that it can:
    // - send messages back to this app
    // - subscribe/unsubscribe the token from topics
    sendTokenToServer = (currentToken) => {
        if (!this.isTokenSentToServer()) {
            console.log('Sending token to server...');
            // TODO(developer): Send the current token to your server.
            this.setTokenSentToServer(true);
        } else {
            console.log('Token already sent to server so won\'t send it again ' +
                'unless it changes');
        }
    }

    isTokenSentToServer = () => {
        return window.localStorage.getItem('sentToServer') === '1';
    }

    setTokenSentToServer = (sent) => {
        window.localStorage.setItem('sentToServer', sent ? '1' : '0');
    }

    showToken = (currentToken) => {
        alert(currentToken);
    }

    render() {
        const {initializing} = this.state;
        if (initializing) {
            return (
                <Container text style={{marginTop: '7em'}}>
                    <Segment>
                        <Dimmer active inverted page>
                            <Loader size='large'>Loading</Loader>
                        </Dimmer>
                    </Segment>
                </Container>
            )
        }

        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}

const mapStateToProps = ({firebase}) => ({
    firebaseToken: firebase.token
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        storeToken,
        processMessage
    }, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps)(Initializer);