import {
    EVENT_ADD_PARTICIPANT, EVENT_ROUND_ENDED,
    EVENT_ROUND_STARTED, EVENT_VOTE_RECEIVED,
    PROCESS_FCM_MESSAGE,
    PROCESS_FCM_MESSAGE_FAILURE,
    STORE_FCM_TOKEN
} from './actionTypes'


export function storeToken(payload) {
    return {
        type: STORE_FCM_TOKEN,
        payload
    };
}

export function processMessage(payload) {
    return {
        type: PROCESS_FCM_MESSAGE,
        payload
    };
}

export function processMessageFailure(payload) {
    return {
        type: PROCESS_FCM_MESSAGE_FAILURE,
        payload
    };
}

export function processMessage_AddParticipant(payload) {
    return {
        type: EVENT_ADD_PARTICIPANT,
        payload
    };
}

export function processMessage_StartRound(payload) {
    return {
        type: EVENT_ROUND_STARTED,
        payload
    };
}

export function processMessage_EndRound(payload) {
    return {
        type: EVENT_ROUND_ENDED,
        payload
    };
}

export function processMessage_VoteReceived(payload) {
    return {
        type: EVENT_VOTE_RECEIVED,
        payload
    };
}