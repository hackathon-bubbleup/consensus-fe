import React from 'react';
import store, {history} from "./store";
import {ConnectedRouter} from "connected-react-router";
import {Provider} from 'react-redux'
import {Route} from "react-router-dom";
import Home from "./containers/home";
import Presenter from "./containers/presenters";
import PresenterSession from "./containers/presenters/session";
import PlayerSession from "./containers/home/session";
import FirebaseWrapper from "./FirebaseWrapper";
import {Switch} from "react-router";


class Initializer extends React.Component {

    render() {

        return (
            <Provider store={store}>
                <FirebaseWrapper>
                    <ConnectedRouter history={history}>
                        <Switch>
                            <Route exact path="/" component={Home}/>
                            <Route exact path="/present" component={Presenter}/>
                            <Route path="/present/:sessionName" component={PresenterSession}/>
                            <Route path="/:sessionName" component={PlayerSession}/>
                        </Switch>
                    </ConnectedRouter>
                </FirebaseWrapper>
            </Provider>
        );
    }
}

export default Initializer;