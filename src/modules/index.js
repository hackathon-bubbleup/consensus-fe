import {combineReducers} from 'redux'
import sessions from "./sessions";
import participant from "./participantReducer";
import firebaseReducer from "./firebaseReducer";

export default combineReducers({
    sessions,
    participant,
    firebase: firebaseReducer
})