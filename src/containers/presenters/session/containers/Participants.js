import React from "react";
import {Icon, List, Message, Segment} from "semantic-ui-react";
import connect from "react-redux/es/connect/connect";


class Participants extends React.Component {
    render() {
        const {participants} = this.props;
        if (participants.length === 0) {
            return (
                <Segment attached>
                    <Message icon>
                        <Icon name='circle notched' loading/>
                        <Message.Content>
                            <Message.Header>Just a minute</Message.Header>
                            Waiting for participants
                        </Message.Content>
                    </Message>
                </Segment>
            )
        }

        return (
            <Segment attached>
                <List divided verticalAlign='middle'>
                    {
                        participants.map(p => {
                            return (
                                <List.Item key={p.nickname}>
                                    <List.Content>
                                        <List.Header>{p.nickname}</List.Header>
                                    </List.Content>
                                </List.Item>
                            )
                        })
                    }
                </List>
            </Segment>
        );
    }
}

const mapStateToProps = ({sessions}) => ({
    participants: (sessions.session && sessions.session.participants) || []
});

const mapDispatchToProps = (dispatch) => {
    return {}
};


export default connect(mapStateToProps, mapDispatchToProps)(Participants);