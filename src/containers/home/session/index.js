import React from "react";
import {Container, Grid, Menu} from "semantic-ui-react";
import connect from "react-redux/es/connect/connect";
import {Route, Switch} from "react-router";
import Vote from "./containers/Vote";
import Lobby from "./containers/Lobby";
import ProgressResult from "./containers/ProgressResult";

class PlayerSession extends React.Component {
    render() {
        return (
            <div>
                <Menu fixed='top' inverted>
                    <Container>
                        <Menu.Item header>
                            CON-SEN-SUS
                        </Menu.Item>
                    </Container>
                </Menu>
                <Container fluid style={{paddingTop: '2em'}}>
                    <Grid celled style={{minHeight: 600}}>
                        <Grid.Row>
                            <Grid.Column width={16}>
                                <Switch>
                                    <Route exact path={`${this.props.match.path}`} component={Lobby}/>
                                    <Route exact path={`${this.props.match.path}/round`} component={Vote}/>
                                    <Route exact path={`${this.props.match.path}/round/result`} component={ProgressResult}/>
                                </Switch>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Container>
            </div>

        );
    }
}

const mapStateToProps = ({sessions}) => ({
    session: sessions.session
});

const mapDispatchToProps = (dispatch) => {
    return {}
};


export default connect(mapStateToProps, mapDispatchToProps)(PlayerSession);