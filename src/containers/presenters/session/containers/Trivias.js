import React from "react";
import {Button, Form, Header, Icon, Input, Segment} from "semantic-ui-react";
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import {createTrivia} from "../../../../actions/sessionActions";
import {push} from "connected-react-router";

class Trivias extends React.Component {
    constructor(props) {
        super(props);
        this.startRound = this._startRound.bind(this);
        this.addAnswer = this._addAnswer.bind(this);
        this.removeAnswer = this._removeAnswer.bind(this);
        this.state = {
            question: "",
            answers: [
                "", "", ""
            ]
        };
    }

    componentDidUpdate(prevProps){
        if (prevProps.sessionState !== this.props.sessionState && this.props.sessionState === "LOCKED"){
            this.props.push(`${this.props.match.url}/round`);
        }
    }

    _startRound = () => {
        const {createTrivia, match} = this.props;
        const {question, answers} = this.state;
        createTrivia({
            sessionId: match.params.sessionName,
            trivia: {
                question,
                answers: answers.map((val, idx) => ({
                    index: idx,
                    text: val
                }))
            }
        })
    };

    _updateAnswer = (event, data, index) => {
        let newAnswers = this.state.answers;
        newAnswers[index] = data.value;
        this.setState({
            answers: [...newAnswers]
        })
    };

    _addAnswer = () => {
        this.setState({
            answers: [...this.state.answers, ""]
        })
    };

    _removeAnswer = (index) => {
        let newAnswers = this.state.answers;
        newAnswers.splice(index, 1);
        this.setState({
            answers: [...newAnswers]
        })
    };

    render() {
        const {question, answers} = this.state;
        return (
            <div>
                <Segment attached>

                    <Header as='h4' attached={'top'}>
                        <Icon name='question circle'/>
                        <Header.Content>Question</Header.Content>
                    </Header>
                    <Segment attached>
                        <Input fluid
                               value={question}
                               icon={<Icon name='question' inverted circular link/>}
                               placeholder='What would you like to ask...'
                               onChange={(event, data) => this.setState({question: data.value})}
                        />
                    </Segment>

                    <Header as='h4' attached={'top'}>
                        <Icon name='hashtag'/>
                        <Header.Content>Answers</Header.Content>
                    </Header>
                    <Segment attached>
                        <Form>
                            {answers.map((val, index) => {
                                return (
                                    <Form.Field key={index}>
                                        <Input fluid labelPosition='right' type='text'
                                               placeholder={`Question ${index+1}`}
                                               value={val}
                                               onChange={(event, data) => this._updateAnswer(event, data, index)}
                                               action={{
                                                   color: 'red',
                                                   icon: 'remove',
                                                   onClick: () => this.removeAnswer(index)
                                               }}
                                        />
                                    </Form.Field>
                                )
                            })}
                        </Form>
                    </Segment>
                    <Segment attached='bottom' clearing>
                        <Button floated='left' color={'blue'} onClick={this.addAnswer}>Add Answer</Button>
                    </Segment>

                </Segment>
                <Segment attached='bottom' clearing>
                    <Button floated='right' color={'teal'} size='massive' onClick={this.startRound}>
                        Start Round
                    </Button>
                </Segment>
            </div>
        );
    }
}

const mapStateToProps = ({sessions}) => ({
    participants: (sessions.session && sessions.session.participants) || [],
    sessionState: (sessions.session && sessions.session.status) || []
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        createTrivia,
        push
    }, dispatch)
};


export default connect(mapStateToProps, mapDispatchToProps)(Trivias);